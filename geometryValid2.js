// Import readline
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});


function tube(side) {
    return side**3;
}

rl.question(
    'Input the side : ',
     function(side) { 

        if(isNaN(side)) {
            console.log(`${side}' is not a number!`)
        } else {
            var volume = tube(side); 
            console.log('The volume :' + volume) 
        }

    })